package cursoSpring.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.Binding;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cursoSpring.app.model.Banner;
import cursoSpring.app.model.Pelicula;
import cursoSpring.app.service.IBannerService;
import cursoSpring.app.utils.Utileria;

@Controller
@RequestMapping(value="/banners")
public class BannerController {

	@Autowired
	private IBannerService bannerService;
	
	@GetMapping(value = "/index")
	public String mostrarIndex(@ModelAttribute Banner banner,Model model) {
		
		List<Banner> lstBanner = bannerService.BuscarTodas();
		model.addAttribute("lstBanner", lstBanner);
		
		return "banners/listBanner";
	}
	
	@GetMapping(value="/create")
	public String crear(@ModelAttribute Banner banner,Model model){
		return "banners/formBanner";
	}
	
	@GetMapping(value="/edit/{id}")
	public String editar(Model model,@PathVariable("id") int idBanner) {
		
		Banner banner = bannerService.buscarPorID(idBanner);
		model.addAttribute("banner", banner);
		return "banners/formBanner";
	}
	
	@PostMapping(value="/delete/{id}")
	public String eliminar(@PathVariable("id") int idBanner) {
		
		bannerService.eliminar(idBanner);
		return "redirect:banners/listBannner";
	}
	
	@PostMapping(value="/save")
	public String guardar(@ModelAttribute Banner banner,BindingResult result,RedirectAttributes attributes,
			@RequestParam("archivoImagen") MultipartFile multiPart,HttpServletRequest request) {
		
		System.out.println("Configurando Data Binding con Pelicula " + banner);
		
		if(result.hasErrors()) {
			System.out.println("Ocurriero Errores");
			
			return "/banners/formBanner";
		}
		
		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request);
			banner.setArchivo(nombreImagen);
		}
		banner.getId();
		
		bannerService.insertar(banner);
		attributes.addFlashAttribute("msg", "Objeto insertado Correctamente");
		return "redirect:/banners/index";
	}
	
	@ModelAttribute("lstBanner")
	public List<Banner> getLstPeliculasActivas(){
		return bannerService.BuscarTodas();
	}
	
//	@InitBinder
//	public void initBinder(WebDataBinder binder) {
//		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
//	}
	
}
