package cursoSpring.app.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cursoSpring.app.model.Noticia;

@Repository
public interface NoticiasRepository extends JpaRepository<Noticia, Integer> {
//public interface NoticiasRepository extends CrudRepository<Noticia, Integer> {cursoSprint.app.pruebajpa usaba este extends
	
	//SELECT * FROM Noticias WHERE Status = ? 
	List<Noticia> findByEstatus(String status);
	
	//WHERE Fecha = ? -- Arreglar el tema de que no encuentra por que toma la hora. 
	List<Noticia> findByFecha(LocalDate fecha);
	
	//WHERE Estatus = ? AND Fecha = ?
	List<Noticia> findByEstatusAndFecha(String estatus, LocalDate fecha);

	//WHERE Estatus = ? OR Fecha = ?
	List<Noticia> findByEstatusOrFecha(String estatus, LocalDate fecha);
	
	//WHERE Fecha BETWEEN ? AND ? 
	List<Noticia> findByFechaBetween(LocalDate fechaDesde, LocalDate fechaHasta);
}
