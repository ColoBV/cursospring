package pruebasInterfazJpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppPaging {

	public static void main(String[] args) {
		// FindAll con Paginacion para mejorar el rendimiento de la pagina. 
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
//		Page<Noticia> listaNoticia = repo.findAll(PageRequest.of(0, 5));//Recibe un objeto del tipo Pageable sin ordenamiento
		Page<Noticia> listaNoticia = repo.findAll(PageRequest.of(0, 5,Sort.by("titulo")));//Recibe un objeto del tipo Pageable con ordenaminto
		
		context.close();
	}

}
	