package jpaPruebaRelaciones;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Horarios;
import cursoSpring.app.repository.HorariosRepository;

public class AppRepoHorarios {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		HorariosRepository repo = context.getBean("horariosRepository", HorariosRepository.class);
		
		List<Horarios> lista = repo.findAll();
		
		for (Horarios horario : lista) {
			System.out.println(horario);
		}
		
		System.out.println("Cantidad de Entidades: " + lista.size());
		
		context.close();
	}

}
