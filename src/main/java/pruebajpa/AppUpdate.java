package pruebajpa;

import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppUpdate {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		//Primero tengo que buscar la entidad
		Optional<Noticia> opcional = repo.findById(1);
		
		//Updateo la entidad con el metodo
		if(opcional.isPresent()) { //Se fija que la variable Opcional tenga algo
			Noticia noticia = opcional.get();
			noticia.setDetalle("El cura si se culeo al pibe");
			noticia.setEstatus("Inactiva");
			
			repo.save(noticia);
		}
		
		context.close();
	}

}
