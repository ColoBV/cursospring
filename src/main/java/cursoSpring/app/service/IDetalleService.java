package cursoSpring.app.service;

import cursoSpring.app.model.Detalle;

public interface IDetalleService{
	void insertar(Detalle detalle);
	void eliminar(int idDetalle);
}
