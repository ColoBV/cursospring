package cursoSpring.app.service;


import java.time.LocalDate;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cursoSpring.app.model.Horarios;
import cursoSpring.app.model.Pelicula;
import cursoSpring.app.repository.HorariosRepository;
import cursoSpring.app.repository.PeliculaRepository;

@Service
public class PeliculaServiceJPA implements IPeliculaService{
	
	@Autowired
	private PeliculaRepository peliculaRepo;
	
	@Autowired
	private HorariosRepository horariosRepo;
	
	private List<Pelicula> lista = null;
	
	@Override
	public List<Pelicula> buscarTodas() {
		return peliculaRepo.findAll();
	}

	@Override
	public Pelicula buscarPorID(int idPelicula) {
		return peliculaRepo.findById(idPelicula).get();
	}

	@Override
	public void insertar(Pelicula pelicula) {
		// TODO Auto-generated method stub
		peliculaRepo.save(pelicula);
	}

	@Override
	public List<String> buscarGeneros() {
		List<String> generos = new LinkedList<>();
		
		generos.add("Accion");
		generos.add("Aventura");
		generos.add("Clasicas");
		generos.add("Comedia Romantica");
		generos.add("Drama");
		generos.add("Terror");
		generos.add("Infantil");
		generos.add("Accion y Aventura");
		generos.add("Romantica");
		generos.add("Ciencia Ficción");
		
		return generos;
	}

	@Override
	public void eliminar(Integer idPelicula) {
		// TODO Auto-generated method stub
		peliculaRepo.deleteById(idPelicula);
	}

	@Override
	public List<Pelicula> buscarPorFecha(LocalDate fecha) {
		List<Pelicula> peliculas = peliculaRepo.findByfechaEstreno(fecha);
		return peliculas;
	}

	@Override
	public List<Pelicula> buscarActivas() {
		List<Pelicula> peliculas = null;
		peliculas = peliculaRepo.findByEstatus_OrderByTitulo("Activa");
		return peliculas;
	}

}


//Buscamos en la tabla de horarios, [agrupando por idPelicula]
//Codigo sacado de buscarPorFecha
//List<Horarios> horarios = horariosRepo.findByFecha(fecha);		
// Formamos la lista final de Peliculas que regresaremos.
/*for (Horarios h : horarios) {
	// Solo nos interesa de cada registro de horario, el registro de pelicula.
	peliculas.add(h.getPelicula());
}*/		

