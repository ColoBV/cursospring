package cursoSpring.app.service;

import cursoSpring.app.model.Noticia;

public interface INoticiasService {
	void guardar(Noticia noticia);
}
