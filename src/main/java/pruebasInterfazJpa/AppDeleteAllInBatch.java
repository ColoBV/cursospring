package pruebasInterfazJpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.repository.NoticiasRepository;

public class AppDeleteAllInBatch {

	public static void main(String[] args) {
		// Delete all in batch a diferencia de DeleteAll(), es que �ste borra en una sola sentencia. (Mas Eficiente!!)
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		repo.deleteAllInBatch();
		
		context.close();
	}

}
