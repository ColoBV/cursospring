package cursoSpring.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cursoSpring.app.model.Detalle;
import cursoSpring.app.repository.DetallesRepository;

@Service
public class DetalleServiceJPA implements IDetalleService{

	@Autowired
	private DetallesRepository detalleRepo;
	
	@Override
	public void insertar(Detalle detalle) {
		// TODO Auto-generated method stub
		detalleRepo.save(detalle);
	}

	@Override
	public void eliminar(int idDetalle) {
		// TODO Auto-generated method stub
		detalleRepo.deleteById(idDetalle);
	}

}
