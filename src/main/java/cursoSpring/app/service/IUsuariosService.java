package cursoSpring.app.service;

import cursoSpring.app.model.Usuario;

public interface IUsuariosService {
	void insertar(Usuario usuario);
	Usuario buscarPorCuenta(String cuenta);
}
