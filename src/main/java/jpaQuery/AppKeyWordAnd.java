package jpaQuery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppKeyWordAnd {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		try {
		
			SimpleDateFormat formattDate = new SimpleDateFormat("yyyy-MM-dd");
			
			List<Noticia> listaNoticias = repo.findByEstatusAndFecha("Activa",formattDate.parse("2017-09-08"));
			
			for (Noticia noticia : listaNoticias) {
				System.out.println(noticia);
			}
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.close();
	}
}
