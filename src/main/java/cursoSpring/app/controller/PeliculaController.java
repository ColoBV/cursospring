package cursoSpring.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.sun.org.apache.bcel.internal.classfile.Attribute;

import cursoSpring.app.model.Pelicula;
import cursoSpring.app.service.IDetalleService;
import cursoSpring.app.service.IPeliculaService;
import cursoSpring.app.utils.Utileria;

@Controller
@RequestMapping(value = "/peliculas")
public class PeliculaController {

	@Autowired
	private IDetalleService detalleService;
	
	@Autowired
	private IPeliculaService peliculaService;

	@GetMapping(value = "/index")
	public String mostrarIndex(Model model) {

		List<Pelicula> lstPeliculas = peliculaService.buscarTodas();
		model.addAttribute("lstPeliculas", lstPeliculas);

		return "peliculas/listPeliculas";
	}

	@GetMapping(value = "/create")
	public String crear(@ModelAttribute Pelicula pelicula,Model model) {
		
//		List<String> generos = peliculaService.buscarGeneros();
//		model.addAttribute("generos", generos);
		
		return "peliculas/formPeliculas";
	}

	@PostMapping(value = "/save")
	public String guardar(@ModelAttribute Pelicula pelicula, BindingResult result, RedirectAttributes attributes,
			@RequestParam("archivoImagen") MultipartFile multiPart, HttpServletRequest request) {

		System.out.println("Configurando Data Binding con Pelicula " + pelicula);

		if (result.hasErrors()) {
			System.out.println("Hubo errores");

			return "/peliclas/formPeliculas";
		}

		if (!multiPart.isEmpty()) {
			String nombreImagen = Utileria.guardarImagen(multiPart, request);
			pelicula.setImagen(nombreImagen);
		}

		//Se pobla automaticamente con el DataBinding
		detalleService.insertar(pelicula.getDetalle());
		
		peliculaService.insertar(pelicula);

		attributes.addFlashAttribute("msg", "Objeto insertado correctamente");
		return "redirect:/peliculas/index";
	}
	
	@GetMapping(value="/edit/{id}")
	public String editar(@PathVariable("id") int idPelicula,Model model) {
		Pelicula pelicula = peliculaService.buscarPorID(idPelicula);
		
		model.addAttribute("pelicula", pelicula);
		return "/peliculas/formPeliculas";
	}
	
	@GetMapping(value="/delete/{id}")
	public String eliminar(@PathVariable("id") int idPelicula,RedirectAttributes attribute) {
		
		Pelicula pelicula = peliculaService.buscarPorID(idPelicula);
		
		peliculaService.eliminar(idPelicula);
		detalleService.eliminar(pelicula.getDetalle().getId());
		attribute.addAttribute("mensaje", "Se ha Borrado con exito");
		return "redirect:/peliculas/index";
		
	}

	//Para no tener que repetir el metodo de cargar el selector con los generos. Se puede crear un metodo a nivel metdo. Esto se ejecutara
	//Cada vez que se llame a los metodos de �sta clase. 
	@ModelAttribute("generos")
	public List<String> getGeneros(){
		return peliculaService.buscarGeneros();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
}
