package pruebajpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.repository.NoticiasRepository;

public class AppDelete {

	public static void main(String[] args) {
		// Operacion CRUD - Delte. Borrar
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		int idNoticia = 2;
		if(repo.existsById(idNoticia)) {			
			repo.deleteById(idNoticia);
		}
		
		context.close();
	}

}
