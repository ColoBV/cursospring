package jpaPruebaRelaciones;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Detalle;
import cursoSpring.app.repository.DetallesRepository;


public class AppDetalleFindAll {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		DetallesRepository repo = context.getBean("detallesRepository", DetallesRepository.class);
		
		List<Detalle> lista = repo.findAll();
		
		for (Detalle detalle : lista) {
			System.out.println(detalle);
		}
		
		context.close();
	}

}
