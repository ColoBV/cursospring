package cursoSpring.app.repository;



import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cursoSpring.app.model.Pelicula;

@Repository
public interface PeliculaRepository extends JpaRepository<Pelicula, Integer> {

	List<Pelicula> findByfechaEstreno(LocalDate localDateTime);
	List<Pelicula> findByEstatus_OrderByTitulo(String activa);
}
