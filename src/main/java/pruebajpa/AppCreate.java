package pruebajpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.model.Pelicula;
import cursoSpring.app.repository.NoticiasRepository;
import cursoSpring.app.repository.PeliculaRepository;

public class AppCreate {

	//Aplicacion para persitir (Crear) en la tabla Noticias un objeto de tipo Noticias
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		PeliculaRepository repopeli = context.getBean("peliculaRepository", PeliculaRepository.class);
		
		
//		Noticia noticia = new Noticia();
//		noticia.setTitulo("Espero que el enano se le rompa el otro brazo");
//		noticia.setDetalle("Las piernas tambien estarian bien");
		
		Pelicula peli = new Pelicula();
		peli.setDuracion(180);
		peli.setTitulo("Juanito y los clonosaurios 2");
		peli.setClasificacion("C");
		peli.setGenero("Drama");
		
//		repo.save(noticia);
		repopeli.save(peli);
		context.close();
	}

}
