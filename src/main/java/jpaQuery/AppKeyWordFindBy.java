package jpaQuery;

import java.time.LocalDateTime;
import java.util.List;
import java.util.TimeZone;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Pelicula;
import cursoSpring.app.repository.PeliculaRepository;

public class AppKeyWordFindBy {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		PeliculaRepository repo = context.getBean("peliculaRepository", PeliculaRepository.class);
//		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
//		SimpleDateFormat formattDate = new SimpleDateFormat("yyyy-MM-dd");
//		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		
//		List<Noticia> lstNoticia = repo.findByFecha(LocalDate.of(2017,10,20));
		//NOTA MENTAL: GENERA UN LOOP INFINITO. Por que Pelicula carga todos los detalles y desde los detalles todas las peliculas y loopea.  
		
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		List<Pelicula> lstPelicula = repo.findByfechaEstreno(LocalDateTime.of(2020,06,13,23,45,14));

		for (Pelicula p : lstPelicula) {
			System.out.println(p);
		}
		context.close();
	}

}
