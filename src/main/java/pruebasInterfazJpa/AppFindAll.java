package pruebasInterfazJpa;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppFindAll {

	public static void main(String[] args) {
		
		// En estas clases se prueba el cambio de extension por JPARepository y no por CrudRepository
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		List<Noticia> noticias = (List<Noticia>) repo.findAll();// este es el cambio significativo. No devuelve un iterabe
		
		for(Noticia n : noticias) 
			System.out.println(n);
		
		
		context.close();
	}

}
