##Este repositorio contiene todo lo aprendido en el Curso de Spring.
 
Los temas aprendidos en el Curso Spring son: 
-Ciclo de vida de las peticiones HTTP.
-Configuracion de FrontController (Dispatcher Servlet)
-Configuracion de ViewResolver.
-En los JSP se uso, JSTL y expresion language.
-Tag de formularios para html y su vinculacion con los modelos (Data Binding)

PERSISTENCIA DE DATOS:
-Configuracion de los Beans.
-Creacion de repositorios para clases de modelos con Spring JPA
-Crear metodos propios para consultas particularizadas(Query Method)
-

---
