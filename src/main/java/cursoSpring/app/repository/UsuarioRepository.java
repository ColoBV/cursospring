package cursoSpring.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import cursoSpring.app.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	Usuario findByCuenta(String cuenta);
}
