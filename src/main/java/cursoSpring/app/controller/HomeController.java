package cursoSpring.app.controller;



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cursoSpring.app.model.Banner;
import cursoSpring.app.model.Horarios;
import cursoSpring.app.model.Noticia;
import cursoSpring.app.model.Pelicula;
import cursoSpring.app.service.IBannerService;
import cursoSpring.app.service.IHorariosService;
import cursoSpring.app.service.INoticiasService;
import cursoSpring.app.service.IPeliculaService;
import cursoSpring.app.utils.Utileria;

@Controller
public class HomeController {
	
	@Autowired
	private IPeliculaService servicePelicula;
	@Autowired
	private IBannerService serviceBanner;
	@Autowired 
	private IHorariosService serviceHorario;
	@Autowired
	private INoticiasService serviceNoticia;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private List<Pelicula> peliculas;
	
	//Deberia estar el acerca por aca
	
	@RequestMapping(value="/formLogin",method = RequestMethod.GET)
	public String mostarLogin() {
		return "formLogin";
	}
	
	@RequestMapping(value="/home",method=RequestMethod.GET)
	public String getHome() {
		return "home";
	}
	
	//Esto simboliza que funciona igual que el index.jsp
	@RequestMapping(value="/",method=RequestMethod.GET)
	 public String mostrarPrincipal(Model model) throws ParseException {
		 
		List<Pelicula> peliculas;
		List<String> lstFechasFiltro;
//		List<Banner> lstBanner;
		
		peliculas = servicePelicula.buscarTodas();
//		lstFechasFiltro = Utileria.getDayForFilters();
//		lstBanner = serviceBanner.BuscarTodas();
		
//		model.addAttribute("lstBanner", lstBanner);
//		model.addAttribute("fechaFiltro", lstFechasFiltro);
		model.addAttribute("fechaBusqueda",dateFormat.format(System.currentTimeMillis()));
		model.addAttribute("lstPeliculas", peliculas);
		 
		return "home";
	 }
	
	@RequestMapping(value = "/search", method=RequestMethod.POST)
	public String buscar(@RequestParam("fecha") String fecha, Model model) throws ParseException {
		// Regresamos la fecha que selecciono el usuario con el mismo formato
		
//		List<String> listaFechas = Utileria.getDayForFilters();
		List<Pelicula> peliculas  = servicePelicula.buscarPorFecha(Utileria.stringToLocalDate(fecha));
//		model.addAttribute("fechaFiltro",listaFechas);			
		model.addAttribute("lstPeliculas", peliculas);			
		return "home";
	
	}
	
	
//	public String mostrarDetalle(Model model,@PathVariable("id") int idPelicula,@PathVariable("fecha") String fechaBusqueda) {
	@RequestMapping(value="/detail/{idPelicula}/{fecha}")
	public String mostrarDetalle(Model model,@PathVariable("idPelicula") int idPelicula,@PathVariable("fecha") String fecha) {
		
		List<Horarios> horarios = serviceHorario.buscarPorIdPeliculaFecha(idPelicula, Utileria.stringToLocalDate(fecha));
		model.addAttribute("horarios",horarios);
		model.addAttribute("fechaBusqueda",fecha);
		model.addAttribute("pelicula",servicePelicula.buscarPorID(idPelicula));
		return "detalle";
	}
	
	@ModelAttribute("noticias")
	public List<Noticia> getNoticias(){
//		return serviceNoticia.buscarUltimas();
		return null;
	}
	
	@ModelAttribute("lstBanner")
	public List<Banner> getBanners(){
		return serviceBanner.BuscarTodas();
	}
	
	@ModelAttribute("fechaFiltro")
	public List<String> getFechaFiltros(){
		return Utileria.getDayForFilters();
	}
	
	/**
	 * Metodo para personalizar el Data Binding para los atributos de tipo Date
	 * @param webDataBinder
	 */
	/*@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {				
		webDataBinder.registerCustomEditor(LocalDate.class, new CustomDateEditor(dateFormat, true));
	}*/
}
