package cursoSpring.app.utils;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

public class Utileria {

	/**
	 * Metodo que regresa una Lista de Strings con las fechas siguientes, segun el parametro count
	 * @param count
	 * @return
	 */
	public static List<String> getNextDays(int count) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		// Today's Date
		Date start = new Date();
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DAY_OF_MONTH, count); // Next N days from now
		Date endDate = cal.getTime();

		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTime(start);
		List<String> nextDays = new ArrayList<String>();
		while (!gcal.getTime().after(endDate)) {
			Date d = gcal.getTime();
			gcal.add(Calendar.DATE, 1);
			nextDays.add(sdf.format(d));
		}
		return nextDays;
	}
	
	public static String guardarImagen(MultipartFile multiPart, HttpServletRequest request) {
		// Obtenemos el nombre original del archivo
		String nombreOriginal = multiPart.getOriginalFilename();
//		Quita espacio del archivo
		nombreOriginal = nombreOriginal.replace(" ", "-");
		// Obtenemos la ruta ABSOLUTA del directorio images
		// apache-tomcat/webapps/cineapp/resources/images/
		String rutaFinal = request.getServletContext().getRealPath("/resources/images/");
		try {
			// Formamos el nombre del archivo para guardarlo en el disco duro
			File imageFile = new File(rutaFinal + nombreOriginal);
			System.out.println(imageFile.getAbsolutePath()); //Obtenemos la ruta completa donde se guarda
			
			// Aqui se guarda fisicamente el archivo en el disco duro
			multiPart.transferTo(imageFile);
			return nombreOriginal;
		} catch (IOException e) {
			System.out.println("Error " + e.getMessage());
			return null;
		}
	}
	
	
	public static Date dateToSQLDate(Date fechaConvertir) {
		java.sql.Date sqlDate = java.sql.Date.valueOf(fechaConvertir.toString());
		return sqlDate;
	}
	
	public static LocalDate stringToLocalDate(String fechaConvertir) {
		LocalDate localDate = LocalDate.parse(fechaConvertir);
		return localDate;
	}
	
	public static List<String> getDayForFilters(){

		List<String> fechasFiltro = new ArrayList<String>();
		
		fechasFiltro.add("2017-10-20");
		fechasFiltro.add("2017-10-22");
		fechasFiltro.add("2017-10-24");
		
		return fechasFiltro;
	}
	
}


