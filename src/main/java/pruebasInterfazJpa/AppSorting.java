package pruebasInterfazJpa;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Sort;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppSorting {

	public static void main(String[] args) {
		// Devuelve los elemntos ordenados por un campo
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
//		List<Noticia> listaNoticias = repo.findAll(Sort.by("titulo").descending());
		List<Noticia> listaNoticias = repo.findAll(Sort.by("titulo").descending().and(Sort.by("id").ascending()));
		
		for(Noticia n : listaNoticias)
			System.out.println(n);
		
		context.close();
	}

}
