package cursoSpring.app.service;

import org.springframework.stereotype.Service;

import cursoSpring.app.model.Noticia;

@Service
public class NoticiaServiceImpl implements INoticiasService {

	@Override
	public void guardar(Noticia noticia) {
		
		System.out.println(noticia);
	}

}
