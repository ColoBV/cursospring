package pruebajpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.repository.NoticiasRepository;

public class AppExists {

	public static void main(String[] args) {
		//Metodo para poder verificar si existe una entidad de manera mas rapida. 
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		int idNoticia = 1;
		System.out.println(repo.existsById(idNoticia));
		
		context.close();
	}

}
