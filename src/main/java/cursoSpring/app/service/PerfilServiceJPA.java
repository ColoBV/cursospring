package cursoSpring.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cursoSpring.app.model.Perfil;
import cursoSpring.app.repository.PerfilRepository;

@Service
public class PerfilServiceJPA implements IPerfilService{

	@Autowired
	private PerfilRepository perfilRepo;
	
	@Override
	public void insertar(Perfil perfil) {
		perfilRepo.save(perfil);
	}

}
