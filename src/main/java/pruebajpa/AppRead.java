package pruebajpa;

import java.util.Optional;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppRead {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		//Operacion de READ del CRUD
		Optional<Noticia> noticia = repo.findById(1);
		
		//Sin el get me devuelve el optional con el noticias adentro
		System.out.println(noticia.get());
		context.close();
	}

}
