package cursoSpring.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cursoSpring.app.model.Banner;
import cursoSpring.app.repository.BannerRepository;

@Service
public class BannerServiceJPA implements IBannerService{

	@Autowired
	private BannerRepository bannerRepo;
	
	@Override
	public List<Banner> BuscarTodas() {	
		return bannerRepo.findAll();
	}

	@Override
	public Banner buscarPorID(int idBanner) {
		Banner banner = null;
		Optional<Banner> bannerOpt = bannerRepo.findById(idBanner);
		
		if(!bannerOpt.isEmpty())
			 banner = bannerOpt.get();
		
		return banner;
	}

	@Override
	public void insertar(Banner banner) {
		bannerRepo.save(banner);
	}

	@Override
	public void eliminar(int idBanner) {
		bannerRepo.deleteById(idBanner);
	}

}
