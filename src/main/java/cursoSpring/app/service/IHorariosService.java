package cursoSpring.app.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import cursoSpring.app.model.Horarios;

public interface IHorariosService {
	List<Horarios> buscarPorIdPeliculaFecha(int idPelicula, LocalDate horario);
	List<Horarios> mostrarTodas();
	Page<Horarios> mostrarTodas(Pageable page);
	Horarios buscarPorID(int idHorario);
	
	
	void insertar(Horarios horario);
	void eliminar(int idHorario);
}
