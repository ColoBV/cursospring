package pruebajpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.repository.NoticiasRepository;

public class AppDeleteAll {

	public static void main(String[] args) {
		// ESTE METODO HACE PORONGA TODA LA TABLA! 
		// MMUUUUUUCHO CUIDADO
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		repo.deleteAll();
		
		context.close();
		
	}

}
