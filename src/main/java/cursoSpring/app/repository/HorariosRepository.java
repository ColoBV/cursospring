package cursoSpring.app.repository;


import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cursoSpring.app.model.Horarios;

@Repository
public interface HorariosRepository extends JpaRepository<Horarios, Integer> {
	//	Referencia a Objeto Completos
	//  Aca puedo hacer todos los querymethos. Lo CRUD ya est�. 
	List<Horarios> findByPelicula_IdAndFechaOrderByHora(int idPelicula, LocalDate fecha);
	List<Horarios> findByFecha(LocalDate fecha);
}
