package cursoSpring.app.service;

import java.util.List;

import cursoSpring.app.model.Banner;

public interface IBannerService {
	List<Banner> BuscarTodas();
	Banner buscarPorID(int idBanner);
	
	void insertar(Banner banner);
	void eliminar(int idBanner);
}
