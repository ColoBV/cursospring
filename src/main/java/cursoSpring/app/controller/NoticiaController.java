package cursoSpring.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.service.INoticiasService;

@Controller
public class NoticiaController {
	
	@Autowired
	private INoticiasService noticiaService;
	
	@RequestMapping(value="/create",method = RequestMethod.GET)
	public String crear() {
		return "noticias/formNoticias";
	}
	
	@PostMapping(value="/save")
	public String guardar(Noticia noticia) {
		
		noticiaService.guardar(noticia);
		
		return "noticias/formNoticias";
	}

}
