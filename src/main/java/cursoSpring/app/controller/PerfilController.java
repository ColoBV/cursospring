package cursoSpring.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import cursoSpring.app.model.Perfil;

@Controller
@RequestMapping("/perfiles")
public class PerfilController {
	
	@GetMapping
	public String crearPerfil(@ModelAttribute Perfil perfil) {
		return "perfiles/formPerfil";
	}
	
}
