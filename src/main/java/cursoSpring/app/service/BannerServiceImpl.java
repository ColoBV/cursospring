package cursoSpring.app.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import cursoSpring.app.model.Banner;

//@Service
public class BannerServiceImpl {

	private LinkedList<Banner> lstBanner = null;
	
	public BannerServiceImpl() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
		try {
			
			lstBanner = new LinkedList<>();
			
			Banner banner1 = new Banner();
			banner1.setId(1);
			banner1.setTitulo("Logan");
			banner1.setDescripcion("Logan");
			banner1.setImagen("slide1.jpg");
			banner1.setEstatus("Activa");
			banner1.setFechaPublicacion((Date) formatter.parse("13-02-2021"));
			
			Banner banner2 = new Banner();
			banner2.setId(2);
			banner2.setTitulo("Logan");
			banner2.setDescripcion("Logan");
			banner2.setImagen("slide2.jpg");
			banner2.setEstatus("Activa");
			banner2.setFechaPublicacion((Date) formatter.parse("14-02-2021"));
			
			Banner banner3 = new Banner();
			banner3.setId(3);
			banner3.setTitulo("Logan");
			banner3.setDescripcion("Logan");
			banner3.setImagen("slide3.jpg");
			banner3.setEstatus("Activa");
			banner3.setFechaPublicacion((Date) formatter.parse("15-02-2021"));
			
			Banner banner4 = new Banner();
			banner4.setId(4);
			banner4.setTitulo("Logan");
			banner4.setDescripcion("Logan");
			banner4.setImagen("slide4.jpg");
			banner4.setEstatus("Inactiva");
			banner4.setFechaPublicacion((Date) formatter.parse("16-02-2021"));
			
			lstBanner.add(banner1);
			lstBanner.add(banner2);
			lstBanner.add(banner3);
			lstBanner.add(banner4);
			
		} catch (ParseException e) {
			System.out.println("Error: "+ e.getMessage());
		}
	}
	
	@Override
	public List<Banner> BuscarTodas() {
		return lstBanner;
	}

	@Override
	public void insertar(Banner banner) {
		lstBanner.add(banner);
	}

}
