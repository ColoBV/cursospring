<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Soy el Home</title>
<spring:url value="/resources" var="urlPublic" />
<link rel="stylesheet"
	href="${urlPublic}/bootstrap/css/bootstrap.min.css">
<!-- 		Aca le digo al a JSP donde tiene que buscar los recursos css,js,etc -->
</head>
<body>
	<div class="card">
		<div class="card-header">Lista Peliculas</div>
		<div class="card-body">
			<table class="table table-striped">
				<thead>
					<th>ID</th>
					<th>Titulo</th>
					<th>Duraciòn</th>
					<th>Clasificacion</th>
					<th>Genero</th>
					<th>Imagen</th>
					<th>Fecha Estreno</th>
					<th>Estatus</th>
				</thead>
				<tbody>
					<c:forEach items="${lstPeliculas}" var="pelicula">
						<tr>
							<td>${pelicula.id }</td>
							<td>${pelicula.titulo }</td>
							<td>${pelicula.duracion }min.</td>
							<td>${pelicula.clasificacion }</td>
							<td>${pelicula.genero }</td>
							<td><img src="${urlPublic}/images/${pelicula.imagen }"> </td>
							<td><fmt:formatDate value="${pelicula.fechaEstreno }" pattern="dd/mm/yyyy"/> </td>
<!-- 							Es el if de spring framework -->
							<td>
								<c:choose>
									<c:when test="${pelicula.estatus=='Activa' }">
										<span class="badge badge-success">ACTIVA</span>
								   	</c:when>
									<c:otherwise>
										<span class="badge badge-danger">INACTIVA</span>
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>