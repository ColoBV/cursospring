package cursoSpring.app.service;



import java.time.LocalDate;
import java.util.List;

import cursoSpring.app.model.Pelicula;

public interface IPeliculaService {
	List<Pelicula> buscarTodas();
	Pelicula buscarPorID(int idPelicula);
	List<Pelicula> buscarPorFecha(LocalDate fecha);
	void insertar(Pelicula pelicula);
	List<String> buscarGeneros();
	void eliminar(Integer idPelicula);
	List<Pelicula> buscarActivas();
	
}
