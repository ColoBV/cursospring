package cursoSpring.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cursoSpring.app.model.Perfil;

@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Integer> {

}
