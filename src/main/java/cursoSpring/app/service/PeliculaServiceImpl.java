package cursoSpring.app.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import java.text.ParseException;

import cursoSpring.app.model.Pelicula;

//Declara clase de servicio. 
//@Service
//Se depreca la clase por la impelementacion con Detalles JPA
public class PeliculaServiceImpl {

	private List<Pelicula> lista = null;
	
	public PeliculaServiceImpl() {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-mm-yyyy");
		
		try {
			
			lista = new LinkedList<>();
			
			Pelicula pelicula1 = new Pelicula();
			pelicula1.setId(1);
			pelicula1.setTitulo("IT");
			pelicula1.setGenero("Terror");
			pelicula1.setDuracion(154);
			pelicula1.setClasificacion("PG 16");
			//pelicula1.setFechaEstreno((Date) formatter.parse("13-02-2021"));
			
			Pelicula pelicula2 = new Pelicula();
			pelicula2.setId(2);
			pelicula2.setTitulo("IT");
			pelicula2.setGenero("Terror");
			pelicula2.setDuracion(254);
			pelicula2.setClasificacion("PG 26");
			//pelicula2.setFechaEstreno((Date) formatter.parse("23-02-2022"));
			
			Pelicula pelicula3 = new Pelicula();
			pelicula3.setId(3);
			pelicula3.setTitulo("SAW");
			pelicula3.setGenero("Terror/Thriller");
			pelicula3.setDuracion(354);
			pelicula3.setClasificacion("PG 36");
//			pelicula3.setFechaEstreno((Date) formatter.parse("22-02-2023"));
			
			
			Pelicula pelicula4 = new Pelicula();
			pelicula4.setId(4);
			pelicula4.setTitulo("Parasite");
			pelicula4.setGenero("Suspenso");
			pelicula4.setDuracion(204);
			pelicula4.setClasificacion("PG 16");
			pelicula4.setEstatus("Inactiva");
//			pelicula4.setFechaEstreno((Date) formatter.parse("22-02-2023"));
			
			lista.add(pelicula1);
			lista.add(pelicula2);
			lista.add(pelicula3);
			lista.add(pelicula4);
			
		} catch (ParseException e) {
			System.out.println("Error: "+ e.getMessage());
		}
	}
	
	@Override
	public List<Pelicula> buscarTodas() {
		return lista;
	}

	@Override
	public Pelicula buscarPorID(int idPelicula) {
		
		for(Pelicula p : lista) {
			if(p.getId() == idPelicula) {
				return p;
			}
		}
		return null;
	}

	@Override
	public void insertar(Pelicula pelicula) {
	
		lista.add(pelicula);
		
	}

	@Override
	public List<String> buscarGeneros() {
		List<String> generos = new LinkedList<>();
		
		generos.add("Accion");
		generos.add("Aventura");
		generos.add("Clasicas");
		generos.add("Comedia Romantica");
		generos.add("Drama");
		generos.add("Terror");
		generos.add("Infantil");
		generos.add("Accion y Aventura");
		generos.add("Romantica");
		generos.add("Ciencia Ficción");
		
		return generos;
	}

	@Override
	public void eliminar() {
		// TODO Auto-generated method stub
	
	}
		
}
