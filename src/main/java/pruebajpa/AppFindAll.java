package pruebajpa;

import java.util.LinkedList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppFindAll {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		//Recupera todo los registros de la clase.
		/*
		Iterable<Noticia> it = repo.findAll();
		for(Noticia n : it) {
			System.out.println(n);
		}*/
		
		//FindAllByID
		List<Integer> lstInteger = new LinkedList<Integer>();
		lstInteger.add(2);
		lstInteger.add(3);
		lstInteger.add(4);
		Iterable<Noticia> itNoticias = repo.findAllById(lstInteger);
		
		for(Noticia n : itNoticias) {
			System.out.println(n);
		}
		context.close();
	}

}
