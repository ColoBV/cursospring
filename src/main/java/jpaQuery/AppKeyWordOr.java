package jpaQuery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Noticia;
import cursoSpring.app.repository.NoticiasRepository;

public class AppKeyWordOr {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		try {
		
			SimpleDateFormat formattDate = new SimpleDateFormat("yyyy-MM-dd");
			
			List<Noticia> listaNoticias = repo.findByEstatusOrFecha("Activa",formattDate.parse("2017-09-07 21:00:00.0"));
			
			for (Noticia noticia : listaNoticias) {
				System.out.println(noticia);
			}
		
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.close();
	}

}
