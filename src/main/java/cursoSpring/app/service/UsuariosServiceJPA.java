package cursoSpring.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cursoSpring.app.model.Usuario;
import cursoSpring.app.repository.UsuarioRepository;

@Service
public class UsuariosServiceJPA implements IUsuariosService{

	@Autowired
	private UsuarioRepository usuarioRepo;
	
	@Override
	public void insertar(Usuario usuario) {
		usuarioRepo.save(usuario);
		
	}

	@Override
	public Usuario buscarPorCuenta(String cuenta) {
		return usuarioRepo.findByCuenta(cuenta);
	}

}
