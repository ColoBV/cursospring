package pruebajpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppConexion {

	public static void main(String[] args) {
		//La siguiente variable crea una variable de la instancia de nuestro aplication-context. Con todos los valores
		//Por default en la carpeta resources se leen los siguientes archivos.
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		
		context.close();
	}

}
