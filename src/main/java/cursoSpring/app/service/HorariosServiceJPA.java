package cursoSpring.app.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import cursoSpring.app.model.Horarios;
import cursoSpring.app.repository.HorariosRepository;

@Service
public class HorariosServiceJPA implements IHorariosService{

	@Autowired
	private HorariosRepository horarioRepo;
	
	@Override
	public List<Horarios> buscarPorIdPeliculaFecha(int idPelicula, LocalDate fecha) {
		List<Horarios> lstHorarios = horarioRepo.findByPelicula_IdAndFechaOrderByHora(idPelicula, fecha);
		return lstHorarios;
	}

	@Override
	public void insertar(Horarios horario) {
		// TODO Auto-generated method stub
		horarioRepo.save(horario);
	}

	@Override
	public List<Horarios> mostrarTodas() {
		List<Horarios> lstHorarios = horarioRepo.findAll();
		return lstHorarios;
	}

	@Override
	public Page<Horarios> mostrarTodas(Pageable page) {
		Page<Horarios> lstHorarios = horarioRepo.findAll(page);
		return lstHorarios;
	}
	
	@Override
	public Horarios buscarPorID(int idHorario) {
		Horarios horario = null;
		Optional<Horarios> horarioOpt = horarioRepo.findById(idHorario);
		if(!horarioOpt.isEmpty()) {
			horario = horarioOpt.get();
		}
		
		return horario;
	}

	@Override
	public void eliminar(int idHorario) {
		horarioRepo.deleteById(idHorario);
	}


}
