package jpaPruebaRelaciones;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.model.Pelicula;
import cursoSpring.app.repository.PeliculaRepository;

public class AppFindAll {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		PeliculaRepository repo = context.getBean("peliculaRepository", PeliculaRepository.class);
		
		//Curso: Tiro error por que no existe una columna detalle en la tabla Peliculas
		List<Pelicula> lista = repo.findAll();
		
		for (Pelicula peli : lista) {
			System.out.println(peli);
		}
		
		context.close();

	}

}
