package cursoSpring.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cursoSpring.app.model.Perfil;
import cursoSpring.app.model.Usuario;
import cursoSpring.app.service.IPerfilService;
import cursoSpring.app.service.IUsuariosService;

@Controller
@RequestMapping("/usuarios")
public class UsuariosController {
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private IUsuariosService usuarioService;
	
	@Autowired
	private IPerfilService perfilService;
	
	@GetMapping(value="crear")
	public String crearUsuario(@ModelAttribute Usuario usuario) {
		
		return "/usuarios/formUsuario";
	}
	
	@PostMapping(value="save")
	public String guardarUsuario(@ModelAttribute Usuario usuario,@RequestParam("perfil") String perfil) {
		
		String tmpPass = usuario.getPwd();
		String pwdEncripada = encoder.encode(tmpPass);
		
		usuario.setPwd(pwdEncripada);
		usuario.setActivo(1);
		usuarioService.insertar(usuario);
		
		Perfil perfilTmp = new Perfil();
		perfilTmp.setCuenta(usuario.getCuenta());
		perfilTmp.setPerfil(perfil);
		
		perfilService.insertar(perfilTmp);
		
		return "redirect:/formLogin";
	}
	
	@GetMapping(value="/demo-bcrypt")
	public String pruebaBcrypt() {
		String password="luis123";
		String encriptado = encoder.encode(password);
		System.out.println("La contraseņa es: "  + encriptado);
		
		return "usuarios/demo";
	}
}
