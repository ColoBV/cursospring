package pruebajpa;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import cursoSpring.app.repository.NoticiasRepository;

public class AppCount {

	public static void main(String[] args) {
		// Metodo Count - Saber cuantos registros hay en una tabla. 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("rootApplicationContext.xml");
		NoticiasRepository repo = context.getBean("noticiasRepository", NoticiasRepository.class);
		
		long num = repo.count();
		System.out.println(num);
		
		context.close();
	}

}
