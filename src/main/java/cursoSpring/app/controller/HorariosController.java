package cursoSpring.app.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.naming.Binding;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import cursoSpring.app.model.Horarios;
import cursoSpring.app.model.Pelicula;
import cursoSpring.app.service.IHorariosService;
import cursoSpring.app.service.IPeliculaService;

@Controller
@RequestMapping("/horarios")
public class HorariosController {

	@Autowired
	IHorariosService horariosService;
	
	@Autowired
	IPeliculaService peliculasService;
	
	@GetMapping(value= "/index")
	public String mostrarIndex(@ModelAttribute Horarios horario,Model model) {
		
		model.addAttribute("lstHorarios", horariosService.mostrarTodas());
		return "horarios/listHorarios";
	}
	
	@GetMapping(value= "/indexPageable")
	public String mostrarIndexPageable(Model model,Pageable page) {
		Page<Horarios> lstHorarios = horariosService.mostrarTodas(page);
		model.addAttribute("lstHorarios", horariosService.mostrarTodas(page));
		return "horarios/listHorarios";
	}
	
	
	@GetMapping("/create")
	public String crear(@ModelAttribute Horarios horario) {
		
		return "horarios/formHorarios";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(Model model,@PathVariable("id") int idHorario) {
		
		Horarios horario = horariosService.buscarPorID(idHorario);
		model.addAttribute("horario", horario);
		return "/horarios/formHorarios";
	}
	
	@PostMapping("/save")
	public String guardar(@ModelAttribute Horarios horario, BindingResult result, Model model, RedirectAttributes attributes) {
		
		if(result.hasErrors()) {
			System.out.println("Hubo errores al guardar");
			return "/horarios/formHorarios";
		}
		
		horariosService.insertar(horario);
		attributes.addFlashAttribute("msg", "Insertado correctamente");
		
		return "redirect:/horarios/index";
	}
	
	@GetMapping("/delete/{id}")
	public String eliminar(@PathVariable("id") int idHorario,RedirectAttributes attribute) {
		
		if(idHorario != 0) {
			horariosService.eliminar(idHorario);
			attribute.addFlashAttribute("msg", "Se borro correctamente el horario");
		}else {
			attribute.addFlashAttribute("msg","El id no puede ser vacio");
		}
		//TODO: Falta corregir el formCreate que tiro error al ingresar.
		return "redirect:/horarios/index";
	}
	
	
	@ModelAttribute("lstPeliculas")
	public List<Pelicula> getLstPeliculasActivas(){
		return peliculasService.buscarActivas();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	

}
